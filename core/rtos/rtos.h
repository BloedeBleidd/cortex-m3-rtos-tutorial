/**
 * \file			rtos.h
 * \brief			RTOS API
 *
 * \dir             app
 * \brief           RTOS main directory
 *
 * \date			Nov 19, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

/** @brief   User defined amount of system ticks per second. It does not have to be a millisecond!
 *  @note    Defines the RTOS overhead on CPU performance.
 *  @warning Too many context switches per second may result in system malfunction.
 */
#define RTOS_TICKS_PER_SECOND (1000U)

/** @brief   Decides whether internal integrity testing asserts are to be called.
 *  @note    If the value is greater than 0, the user must define the "void rtosAssert(bool condition)" function.\n
 *           It is a good idea to disable interrupts to prevent context switching, then print some informations and halt.
 *  @warning Disabled assertions may cause a system crash during API misuse but increase overall performance.
 */
#define RTOS_USE_ASSERT (1)

/** @brief Decides whether the user function is to be called in the beginning of the system tick interrupt.
 *  @note  If the value is greater than 0, the user must define the "void rtosTickHook(void)" function.\n
 *         The function runs before the scheduling tasks algorithm.
 */
#define RTOS_USE_TICK_HOOK (1)

/** @brief   Decides whether the user defined function is to be called in the context of the thread idle.
 *  @note    If the value is greater than 0, the user must define the "void rtosIdleHook(void)" function.\n
 *           The function is called when the system has free computing power.\n
 *           A typical application is to put the CPU to sleep.
 *  @warning Note the idle thread stack size defined by RTOS_IDLE_STACK_SIZE_WORDS macro.
 */
#define RTOS_USE_IDLE_HOOK (1)

#define RTOS_IDLE_STACK_SIZE_WORDS (32U)  //!< User defined stack size for the idle task context

typedef uint32_t RtosTime;  //!< System time type - used for sleep and timeouts
typedef uint8_t RtosPriority;  //!< Task priority - the supported range is from 1 to 32, where idle is 0
typedef uint32_t RtosSemaphore;  //!< Semaphore handler as 32-bit counter

/** @brief Task control block structure
 */
typedef struct {
    void* stackPointer;  //!< Stack pointer, The stack grows towards lower addresses
    uint32_t timeout;    //!< Auxiliary variable to put the thread to sleep
    uint8_t priority;    //!< The stored thread priority
} RtosTask;

/** @brief Initialize the system to work with preemptive RTOS.
 *  @note  At this point, the idle thread with the lowest priority in the system is created.
 */
void rtosInit(void);

/** @brief Set up a system interrupt and start scheduling tasks.
 *  @note  There should be uint32_t SystemCoreClock variable defined in the code containing CPU clock in Hertz.\n
 *         The function should not complete its operation so it is a good idea to test that.
 */
void rtosRun(void);

/** @brief Return the number of ticks of the system interrupt since the start of the rtosRun().
 *  @note  If an interrupt with a higher priority lasted longer than the time of a single tick,\n
 *         the counter may not show the correct value.
 *  @return System time
 */
RtosTime rtosTicks(void);

/** @brief Add another thread to the list of threads.
 *  @note  The user must take care to provide the appropriate memory buffer and the file\n
 *         structure. The function does not allocate memory dynamically.
 *  @param handler Task handler structure
 *  @param task Address of the function with the code of the user's thread
 *  @param priority Priority of the thread. Each task should occupy different priority in {1-32} range
 *  @param stack User-reserved memory to serve as the thread's stack
 *  @param stackSize Stack size in 4-byte words
 */
void rtosTaskCreate(RtosTask* handler, void (*task)(void), RtosPriority priority, void* stack, uint32_t stackSize);

/** @brief Put to sleep the thread execution for the time specified in system ticks.
 *  @param ticks Thread sleep time
 */
void rtosTaskSleep(RtosTime ticks);

/** @brief Put to sleep the thread execution until specified system time.
 *  @param lastTick Time at which the task was last unblocked.\n
 *                  The variable must be initialized with the current time before the first use.
 *  @param ticks Cycle sleep time period
 */
void rtosTaskSleepUntil(RtosTime* const lastTick, RtosTime ticks);

/** @brief Initialize semaphore handler
 *  @param handler Semaphore handler
 */
void rtosSemaphoreCreate(RtosSemaphore* handler);

/** @brief Obtain the semaphore if available and decrease its counter.
 *  @note The function is used both for binary and counting version of the semaphore.
 *  @param handler A handle to the semaphore being taken
 *  @param timeout The time in ticks to wait for the semaphore to become available
 *  @return
 *   - true: Access to the semaphore has been obtained
 *   - false: The tick limit has expired and the semaphore has not become available
 */
bool rtosSemaphoreTake(RtosSemaphore* handler, RtosTime timeout);

/** @brief Release the semaphore treating it as a binary version.
 *  @param handler A handle to the semaphore to give
 *  @return
 *   - true: The semaphore was released
 *   - false: An error occurred.
 */
bool rtosSemaphoreGiveBinary(RtosSemaphore* handler);

/** @brief Release the semaphore treating it as a counting version.
 *  @param handler A handle to the semaphore to give
 *  @param maxCount Maximum counter value. The value must be greater than 0
 *  @return
 *   - true: The semaphore was released
 *   - false: An error occurred.
 */
bool rtosSemaphoreGiveCounting(RtosSemaphore* handler, uint32_t maxCount);

/** @brief Preview the value of the semaphore.
 *  @note  The binary version operates on the values 0 and 1.
 *  @param handler A handler for the read semaphore
 *  @return The value of the semaphore counter
 */
uint32_t rtosSemaphorePeek(RtosSemaphore* handler);

#ifdef __cplusplus
}
#endif
