/**
 * \file			rtos.c
 * \brief			RTOS source
 *
 * \date			Nov 19, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "rtos.h"
#include "main.h"

#if (RTOS_USE_ASSERT > 0)
extern void rtosAssert(bool condition);
#define RTOS_ASSERT(x) rtosAssert(x)  //!< Assert function defined by the user
#else
#define RTOS_ASSERT(x)  //!< Assert function not defined by the user
#endif

#if (RTOS_USE_IDLE_HOOK > 0)
extern void rtosIdleHook(void);
#define RTOS_IDLE_HOOK() rtosIdleHook()  //!< Idle hook function defined by the user
#else
#define RTOS_IDLE_HOOK()  //!< Idle hook function not defined by the user
#endif

#if (RTOS_USE_TICK_HOOK > 0)
extern void rtosTickHook(void);
#define RTOS_TICK_HOOK() rtosTickHook()  //!< Tick hook function defined by the user
#else
#define RTOS_TICK_HOOK()  //!< Tick hook function not defined by the user
#endif

#define RTOS_MAX_PRIORITY (32U)  //!< Maximum supported priority by the RTOS

extern uint32_t SystemCoreClock;  //!< CPU speed in Hz

static volatile RtosTime systemTime;  //!< System tick counter
static RtosTask* volatile current;    //!< Currently executing thread handler
static RtosTask* volatile next;       //!< Next thread to be executed
static RtosTask* thread[RTOS_MAX_PRIORITY + 1U];  //!< List of tasks, taking into account priorities. The idle task is the first element.
static uint32_t ready;       //!< A set of flags indicating whether the thread is ready to run
static uint32_t delayed;     //!< A set of flags indicating whether the thread should sleep
static RtosTask idleThread;  //!< Idle task handler
static uint32_t idleStack[RTOS_IDLE_STACK_SIZE_WORDS];  //!< Idle task stack

#pragma GCC push_options
#pragma GCC optimize("-Ofast")  // Optimize source code for execution speed

/** @brief Execute scheduling algorithm
 */
static void schedule(void)
{
    if (ready) {
        next = *(thread + RTOS_MAX_PRIORITY - __builtin_clz(ready));
    }
    else {
        next = *thread;
    }

    if (next != current) {
        SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
    }
}

/** @brief Idle task function
 */
static void idleThreadFunc(void)
{
    while (1) {
        RTOS_IDLE_HOOK();
    }
}

void rtosInit(void)
{
    rtosTaskCreate(&idleThread, idleThreadFunc, 0U, idleStack, RTOS_IDLE_STACK_SIZE_WORDS);
}

void rtosRun(void)
{
    NVIC_SetPriority(PendSV_IRQn, 0xffU);
    SysTick_Config(SystemCoreClock / RTOS_TICKS_PER_SECOND);
    NVIC_SetPriority(SysTick_IRQn, 0U);
    __disable_irq();
    schedule();
    __enable_irq();
}

RtosTime rtosTicks(void)
{
    return systemTime;
}

void rtosTaskCreate(RtosTask* handler, void (*task)(void), RtosPriority priority, void* stack, uint32_t stackSize)
{
    RTOS_ASSERT(NULL != handler);
    RTOS_ASSERT(NULL != task);
    RTOS_ASSERT(32U >= priority);
    RTOS_ASSERT(NULL != stack);
    RTOS_ASSERT(stackSize);
    uint32_t* sp = (uint32_t*)((((uint32_t)(stack) + stackSize * sizeof(uint32_t)) / 8U) * 8U);  // Double word align
    *(--sp) = 1U << 24U;       // xPSR with Thumb bit enabled
    *(--sp) = (uint32_t)task;  // PC
    sp -= 14U;                 // LR, R12, R3-R0, R11-R4
    handler->stackPointer = sp;
    handler->priority = priority;
    thread[priority] = handler;

    if (priority) {
        ready |= (1U << (priority - 1U));
    }
}

void rtosTaskSleep(RtosTime ticks)
{
    __disable_irq();
    uint32_t bit = (1U << (current->priority - 1U));
    ready &= ~bit;
    delayed |= bit;
    current->timeout = ticks;
    schedule();
    __enable_irq();
}

void rtosTaskSleepUntil(RtosTime* const lastTick, RtosTime ticks)
{
    RTOS_ASSERT(NULL != lastTick);
    __disable_irq();
    const RtosTime systemTicks = systemTime;
    const RtosTime tickToWake = *lastTick + ticks;
    bool toSleep = false;

    if (systemTicks < *lastTick) {
        if ((tickToWake < *lastTick) && (tickToWake > systemTicks)) {
            toSleep = true;
        }
    }
    else {
        if ((tickToWake < *lastTick) || (tickToWake > systemTicks)) {
            toSleep = true;
        }
    }

    *lastTick = tickToWake;

    if (toSleep) {
        uint32_t bit = (1U << (current->priority - 1U));
        ready &= ~bit;
        delayed |= bit;
        current->timeout = ticks;
        schedule();
    }

    __enable_irq();
}

void rtosSemaphoreCreate(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    *handler = 0U;
}

bool rtosSemaphoreTake(RtosSemaphore* handler, RtosTime timeout)
{
    RTOS_ASSERT(NULL != handler);

    do {
        uint32_t tmp = __LDREXW(handler);

        if (tmp) {
            tmp--;
            __DMB();

            if (!(__STREXW(tmp, handler))) {
                __DMB();
                return true;
            }
        }

        if (timeout) {
            timeout--;
            rtosTaskSleep(1U);
        }
    } while (timeout);

    return false;
}

bool rtosSemaphoreGiveBinary(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    return rtosSemaphoreGiveCounting(handler, 1U);
}

bool rtosSemaphoreGiveCounting(RtosSemaphore* handler, uint32_t maxCount)
{
    RTOS_ASSERT(NULL != handler);
    RTOS_ASSERT(1U <= maxCount);
    uint32_t tmp = __LDREXW(handler);

    if (tmp < maxCount) {
        tmp++;
        __DMB();

        if (!(__STREXW(tmp, handler))) {
            __DMB();
            return true;
        }
    }

    return false;
}

uint32_t rtosSemaphorePeek(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    return __LDREXW(handler);
}

/** @brief SysTick Cortex-M interrupt
 */
void SysTick_Handler(void)
{
    systemTime++;
    RTOS_TICK_HOOK();

    for (uint32_t working = delayed; working;) {
        RtosTask* task = *(thread + RTOS_MAX_PRIORITY - __builtin_clz(working));
        uint32_t bit = (1U << (task->priority - 1U));
        task->timeout--;

        if (!(task->timeout)) {
            ready |= bit;
            delayed &= ~bit;
        }

        working &= ~bit;
    }

    __disable_irq();
    schedule();
    __enable_irq();
}

#pragma GCC pop_options

/** @brief PendSV Cortex-M interrupt
 */
__attribute__((naked)) void PendSV_Handler(void)
{
    __asm(
        " CPSID i           \n"  // __disable_irq();
        " LDR   r3,=current \n"  // if (current) {
        " LDR   r2,[r3,#0]  \n"
        " CBZ   r2,restore  \n"
        " PUSH  {r4-r11}    \n"  // push registers
        " LDR   r2,[r3,#0]  \n"  // current->stackPointer = sp;
        " STR.W sp,[r2]     \n"
        " restore:          \n"  // }
        " LDR   r2,=next    \n"  // sp = next->stackPointer;
        " LDR   r1,[r2,#0]  \n"
        " LDR   sp,[r1,#0]  \n"
        " LDR   r2,[r2,#0]  \n"  // current = next;
        " STR   r2,[r3,#0]  \n"
        " POP   {r4-r11}    \n"  // pop registers
        " CPSIE i           \n"  // __enable_irq();
        " BX    lr          \n");
}
