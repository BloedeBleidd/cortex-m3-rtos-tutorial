/**
 * \file			watchdog.c
 * \brief			The highest watchdog layer created for the application layer
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "watchdog.h"
#include "driverWatchdog/driverWatchdog.h"

void watchdogRefresh(void)
{
#ifndef DEBUG
    driverWatchdogRefresh();
#endif
}
