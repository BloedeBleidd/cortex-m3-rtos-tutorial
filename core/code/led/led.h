/**
 * \file			led.h
 * \brief			Status LED API
 *
 * \dir             led
 * \brief           Status LED support
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Turn on status LED
 */
void ledOn(void);

/** @brief Turn of status LED
 */
void ledOff(void);

/** @brief Toogle status LED
 */
void ledToggle(void);

#ifdef __cplusplus
}
#endif
