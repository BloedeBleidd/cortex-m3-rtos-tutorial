/**
 * \file			led.c
 * \brief			Status LED control source file
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "led.h"
#include "driverLed/driverLed.h"

void ledOn(void)
{
    driverLedOn();
}

void ledOff(void)
{
    driverLedOff();
}

void ledToggle(void)
{
    driverLedToggle();
}
