/**
 * \file			app.hpp
 * \brief			The highest main project header file containing application initialization called after system startup
 *
 * \dir             app
 * \brief           Top level application
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Ensures proper initialisation of the tasks and runs them.
 *  @note It is the main handler intermediary between an auto-generated c file and c++ code.\n
 *        Prevents the main.c file extension from being automatically changed each time\n
 *        project files are generated in CubeMX.
 */
void app(void);

#ifdef __cplusplus
}
#endif
