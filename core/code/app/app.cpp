/**
 * \file			app.cpp
 * \brief			The highest main project file that contains RTOS tasks
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "app.hpp"
#include "led/led.h"
#include "main.h"
#include "rtos.h"
#include "watchdog/watchdog.h"

#define TASK_MAIN_STACK_SIZE_BYTES (64U)  //!< Main task stack size

static uint32_t TASK_MAIN_STACK[TASK_MAIN_STACK_SIZE_BYTES];  //!< Main task stack
static RtosTask taskMainHandler;                              //!< Main task handler

#define TASK_SECOND_STACK_SIZE_BYTES (64U)  //!< Second task stack size

static uint32_t TASK_SECOND_STACK[TASK_SECOND_STACK_SIZE_BYTES];  //!< Second task stack
static RtosTask taskSecondHandler;                                //!< Second task handler

#define TASK_WATCHDOG_STACK_SIZE_BYTES (32U)  //!< Watchdog task stack size

static uint32_t TASK_WATCHDOG_STACK[TASK_WATCHDOG_STACK_SIZE_BYTES];  //!< Watchdog task stack
static RtosTask taskWatchdogHandler;                                  //!< Watchdog task handler

#define TASK_LED_STACK_SIZE_BYTES (32U)  //!< LED task stack size

static uint32_t TASK_LED_STACK[TASK_LED_STACK_SIZE_BYTES];  //!< LED task stack
static RtosTask taskLedHandler;                             //!< LED task handler

#define SEMAPHORE_TIMEOUT_TICKS (100U)  //!< Semaphore timeout

static RtosSemaphore semaphoreHandler;  //!< Semaphore handler

/** @brief Task which resets the watchdog every single second is used to reset\n
 *         the device after the suspension of the RTOS
 */
static void taskWatchdog(void);

/** @brief Task of blinking LED with 1 second period used to indicate that the system is working properly
 */
static void taskLed(void);

/** @brief Main project task
 */
static void taskMain(void);

/** @brief Second project task
 */
static void taskSecond(void);

void app(void)
{
    rtosInit();

    rtosTaskCreate(&taskWatchdogHandler, taskWatchdog, 1, TASK_WATCHDOG_STACK, TASK_WATCHDOG_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskLedHandler, taskLed, 5, TASK_LED_STACK, TASK_LED_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskMainHandler, taskMain, 4, TASK_MAIN_STACK, TASK_MAIN_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskSecondHandler, taskSecond, 3, TASK_SECOND_STACK, TASK_SECOND_STACK_SIZE_BYTES);

    rtosRun();
}

static void taskWatchdog(void)
{
    for (;;) {
        watchdogRefresh();
        rtosTaskSleep(1000);
    }
}

static void taskLed(void)
{
    RtosTime lastTime = rtosTicks();

    for (;;) {
        ledOn();
        rtosTaskSleepUntil(&lastTime, 100);
        ledOff();
        rtosTaskSleepUntil(&lastTime, 900);
    }
}

static void taskMain(void)
{
    rtosTaskSleep(550);

    uint32_t cnt = 0;

    while (1) {
        rtosTaskSleep(10);
        rtosSemaphoreGiveBinary(&semaphoreHandler);
        cnt++;
    }
}

static void taskSecond(void)
{
    uint32_t obtainCnt = 0;
    uint32_t timeoutCnt = 0;

    while (1) {
        if (rtosSemaphoreTake(&semaphoreHandler, SEMAPHORE_TIMEOUT_TICKS)) {
            obtainCnt++;
        }
        else {
            timeoutCnt++;
        }

        rtosTaskSleep(2);
    }
}

/** @brief Idle hook
 */
extern "C" void rtosIdleHook(void)
{
    __WFI();
}

/** @brief Tick hook
 */
extern "C" void rtosTickHook(void)
{
    static volatile uint32_t cnt = 0;
    cnt++;
}

/** @brief RTOS assert as a example without creating driver and middleware layers
 */
extern "C" void rtosAssert(bool condition)
{
    if (!condition) {
        __disable_irq();  // Disable interrupts to prevent context switch

        if (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) {
            __asm("BKPT #0");  // Insert break when in debug mode
        }

        CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk;  // Disable TRC
        CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;   // Enable TRC
        DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;             // Disable clock cycle counter
        DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;              // Enable clock cycle counter
        DWT->CYCCNT = 0;                                  // Reset cycle counter

        for (;;) {
            ledToggle();

            uint32_t cycles = (SystemCoreClock / 1000000U) * 1000U * 100U;  // 100ms period
            for (uint32_t start = DWT->CYCCNT; (DWT->CYCCNT - start) < cycles;) {
            }
        }
    }
}
