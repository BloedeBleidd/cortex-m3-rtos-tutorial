/**
 * \file			driverWatchdog.h
 * \brief			Watchdog Driver API
 *
 * \dir             driverWatchdog
 * \brief           Hardware watchdog driver
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Refresh hardware watchdog. Period of the watchdog is approximately 3.3s with tolerance depending on LSI RC oscillator
 */
void driverWatchdogRefresh(void);

#ifdef __cplusplus
}
#endif
