/**
 * \file			driverWatchdog.c
 * \brief			Watchdog Driver dealing with refreshing it using registers
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "driverWatchdog.h"
#include "iwdg.h"
#include "stm32f1xx_hal.h"

void driverWatchdogRefresh(void)
{
    HAL_IWDG_Refresh(&hiwdg);
}
