Cortex-M3 RTOS tutorial - own implementation
===
Status: ![build](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/badges/develop/pipeline.svg)

# Table of Contents

[TOC]

# Introduction

In this tutorial you will have an opportunity to familiarize yourself with an exemplary implementation of real time operating system (RTOS). However, we will restrict this design to contain basic preemtive scheduler, which takes into account the priority of tasks to schedule, along with semaphore, because it is the simplest way to introduce synchronization between tasks. There is STM32CubeIDE project ready to launch in the [repository](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial). 

## RTOS

What is RTOS and what do we need it for? It's a separate piece of software that allocates CPU time for every thread. A single thread is unaware of other threads' existence, at least it looks that way. In a regular fashion, the currently running thread is stopped for some time in order to let some other threads with the same or higher priority do their job. When there is a need to delay the execution of a particular context, many CPU cycles can be spared for other threads while the previous task is in a blocked state. We have a number of ways to synchronize the execution of threads. There are semaphores, mutexes, events, queues, etc., which guarantee safe access to the shared resources while not wasting any more CPU cycles by putting the current thread to sleep, or switching context to another waiting thread, provided that there is an available context to switch to at the moment. If there is not, the idle task can be used to put the CPU into a dormant state. From a human perspective, the whole process happens very quickly. We usually assume that a context is switched every 1 millisecond (any other value is also okay), so we get the illusion of concurrent execution. It only takes a few tens/hundreds of CPU instructions to change the context on demand.

The [illustration](https://www.freertos.org/implementation/a00005.html) below shows a classic case of the execution order of 3 threads with different priorities each:

![](doc/preemption.png)

```quote
At (1) task 1 is executing.
At (2) the kernel suspends (swaps out) task 1 ...
... and at (3) resumes task 2.
While task 2 is executing (4), it locks a processor peripheral for its own exclusive access.
At (5) the kernel suspends task 2 ...
... and at (6) resumes task 3.
Task 3 tries to access the same processor peripheral, finding it locked task 3 cannot continue so suspends itself at (7).
At (8) the kernel resumes task 1.
Etc.
The next time task 2 is executing (9) it finishes with the processor peripheral and unlocks it.
The next time task 3 is executing (10) it finds it can now access the processor peripheral and this time executes until suspended by the kernel.
```

Modern microcontrollers provide support for RTOS implementations by offering a dedicated set of instructions, core execution modes (user or priviliged), or even the way the architecture was designed. It all affects the reaction time for certain events and the energy efficiency of a final application.

The main advantage behind RTOS is the fact that when we write a piece of code, we don't have to bother ourselves with the evergrowing complexity of dependencies in a system, which in turn extends the time needed to complete the project, and that is essential in the modern world. The subsequent pieces of a project can contain their own infinite loops, replacing the timer events that are common in bare metal projects. Timer events have a certain drawback, meaning they can affect the execution time of some other task, i.e., one task can affect the other.

## Examples

There are many commercial versions of RTOSes available on the market, as well as free ones for commercial use. It's worth getting to know some of the most popular ones and seeing what they look like from the inside, because a lot of time and effort were put into them, which resulted in thorough documentation and a quite helpful community. Getting to know their internal design will allow you to better understand the RTOS and make the best use of it.

[FreeRTOS](https://www.freertos.org/index.html)

[embOS](https://www.segger.com/products/rtos/embos)

[RTX](https://www.keil.com/arm/rl-arm/kernel.asp)

[Micrium OS, MicroC/OS-II, MicroC/OS-III](https://www.silabs.com/developers/micrium)

**Note: It is recommended to have at least [basic knowledge](https://en.wikipedia.org/wiki/Real-time_operating_system) of RTOS so that after reading this tutorial, one could understand what was in it**

## Own implementation - Assumptions

In this tutorial, we will create our own implementation. The RTOS will be optimized in terms of both FLASH and RAM occupancy and execution time. The code presented in this tutorial will be concise in volume in order not to obfuscate the goal we will try to achieve.

**There will be a number of features available**

- 32 threads/priorities - *each thread has its own unique priority*.
- Sleep function in the classic version and SleepUntil
- Returning system time value
- Counting and binary semaphores - *Take, GiveBinary, GiveCounting, Peak*.
- Setting the time for a single system tick
- Hook to a function which is an IDLE thread - *CPU sleep*.
- Hook to a function that is run periodically during a context change
- User Asserts
- Support for C/C++

The development platform used in this tutorial will be Cortex-M3 with the ARMv7 architecture, which is contained in a rather old but very popular microcontroller from STMicroelectronics - STM32F103CB. To make it easy for the reader to use the finished project, it is recommended to use the BluePill board and any SWD programmer (ST-Link, J-Link).

![](doc/bluepill.png)

The code will support C11 and C++11 standards, and the application will be based on design in STM32CubeIDE, static code analysis CppCheck and FlawFinder, syntax formatting Clang-Format. The compiler is based on GCC.

# API

Firstly, we will prepare API for our RTOS implementation. There is a header file called **[rtos.h](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/blob/develop/core/rtos/rtos.h)** along with Doxygen documentation in a repository.

The individual pieces of API will be described in the order they appear. 

## Dependency

```c
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

...API

#ifdef __cplusplus
}
#endif
```

In first line we create an include guard for the header file. Ifdefs are used to check if we're using C++, if so we specify the API for C. The RTOS header file depends only on the standard **stdint** and **stdbool** libraries, so it can be easily added to unit tests and does not introduce unnecessary dependencies in the project.

## Macros

```c
/** @brief   User defined amount of system ticks per second. It does not have to be a millisecond!
 *  @note    Defines the RTOS overhead on CPU performance.
 *  @warning Too many context switches per second may result in system malfunction.
 */
#define RTOS_TICKS_PER_SECOND (1000U)

/** @brief   Decides whether internal integrity testing asserts are to be called.
 *  @note    If the value is greater than 0, the user must define the "void rtosAssert(bool condition)" function.\n
 *           It is a good idea to disable interrupts to prevent context switching, then print some informations and halt.
 *  @warning Disabled assertions may cause a system crash during API misuse but increase overall performance.
 */
#define RTOS_USE_ASSERT (1)

/** @brief Decides whether the user function is to be called in the beginning of the system tick interrupt.
 *  @note  If the value is greater than 0, the user must define the "void rtosTickHook(void)" function.\n
 *         The function runs before the scheduling tasks algorithm.
 */
#define RTOS_USE_TICK_HOOK (1)

/** @brief   Decides whether the user defined function is to be called in the context of the thread idle.
 *  @note    If the value is greater than 0, the user must define the "void rtosIdleHook(void)" function.\n
 *           The function is called when the system has free computing power.\n
 *           A typical application is to put the CPU to sleep.
 *  @warning Note the idle thread stack size defined by RTOS_IDLE_STACK_SIZE_WORDS macro.
 */
#define RTOS_USE_IDLE_HOOK (1)

#define RTOS_IDLE_STACK_SIZE_WORDS (32U)  //!< User defined stack size for the idle task context
```

Macros begin with the common prefix **RTOS**.
The user can define the following:
1. The number of system ticks per second - *Cyclical context switches*.
2. Whether assertions should be used to test if the API is being called correctly
3. The function that will be called when the system ticks
4. The function that is used as an IDLE thread
5. The IDLE thread's stack size

The **RTOS_USE** macros take values of **0/1** to determine the use of a particular functionality. The user must define appropriate functions, described by comments, that are globally visible during compilation.

The stack size is specified in words, i.e. **4 bytes** - in the example it is **4B*32=128** bytes.

## Types

```c
typedef uint32_t RtosTime;  //!< System time type - used for sleep and timeouts
typedef uint8_t RtosPriority;  //!< Task priority - the supported range is from 1 to 32, where idle is 0
typedef uint32_t RtosSemaphore;  //!< Semaphore handler as 32-bit counter

/** @brief Task control block structure
 */
typedef struct {
    void* stackPointer;     //!< Stack pointer, The stack grows towards lower addresses
    RtosTime timeout;       //!< Auxiliary variable to put the thread to sleep
    RtosPriority priority;  //!< The stored thread priority
} RtosTask;
```

**RtosTime** is a simple 32 bit variable that stores the time in system ticks. It will be used as an argument to functions with timeout, sleep, or system time functionality.

**RtosTime** defines the priority as an 8 bit number. It was created for interface clarity, because only the range of 1-32 will be used.

**RtosSemaphore** may seem surprising. Semaphore as a regular number? This will be explained later in this tutorial.

**RtosTask** - the classic task structure in RTOS. Attention may be attracted by the very small number of variables. We have a thread **stack**, because we can't do without it. We have the **timeout** variable, which will be used to block the task for a specified time, and also the thread's **priority**.

## Initialization and Start

```c
/** @brief Initialize the system to work with preemptive RTOS.
 *  @note  At this point, the idle thread with the lowest priority in the system is created.
 */
void rtosInit(void);

/** @brief Set up a system interrupt and start scheduling tasks.
 *  @note  There should be uint32_t SystemCoreClock variable defined in the code containing CPU clock in Hertz.\n
 *         The function should not complete its operation so it is a good idea to test that.
 */
void rtosRun(void);
```

**rtosInit** must be called before any of the other functions. It will take care of preparing the local RTOS state, including the IDLE thread and the microcontroller's registers.

**rtosRun** runs the previously registered tasks, starting with the one with the highest priority.

## Tasks

```c
/** @brief Add another thread to the list of threads.
 *  @note  The user must take care to provide the appropriate memory buffer and the file\n
 *         structure. The function does not allocate memory dynamically.
 *  @param handler Task handler structure
 *  @param task Address of the function with the code of the user's thread
 *  @param priority Priority of the thread. Each task should occupy different priority in {1-32} range
 *  @param stack User-reserved memory to serve as the thread's stack
 *  @param stackSize Stack size in 4-byte words
 */
void rtosTaskCreate(RtosTask* handler, void (*task)(void), RtosPriority priority, void* stack, uint32_t stackSize);
```

**rtosTaskCreate** registers a task on the system and updates the ***handler*** structure which is the handler of the task. ***task*** is a user function that does not take or return arguments. Then ***priority*** and ***stack***, which must be taken care of by the user. It may be memory in any address space with Read and Write access, allocated dynamically or defined statically. ***stackSize*** specifies how deep the task stack should be with a resolution down to a word (4 bytes). A task can only be created after ***rtosInit*** and before ***rtosRun***.

```c
/** @brief Put to sleep the thread execution for the time specified in system ticks.
 *  @param ticks Thread sleep time
 */
void rtosTaskSleep(RtosTime ticks);
```

**rtosTaskSleep** puts the thread to sleep for ***ticks*** system ticks. The resolution is 1 tick.

```c
/** @brief Put to sleep the thread execution until specified system time.
 *  @param lastTick Time at which the task was last unblocked.\n
 *                  The variable must be initialized with the current time before the first use.
 *  @param ticks Cycle sleep time period
 */
void rtosTaskSleepUntil(RtosTime* const lastTick, RtosTime ticks);
```

**rtosTaskSleepUntil** remembers the last call time in the ***lastTick*** variable and tries to put the thread to sleep so that the wakeup period is ***ticks*** ticks.

Example 1: *The period is 50 ticks, and the execution time of the user code varies between 10-30 ticks. The thread will be put to sleep for such a time that the code will be called every 50 ticks. In the range of 20-40 ticks.*

Example 2: *The period is 50 ticks and the execution time of the user code is 100 ticks. The thread will not be put to sleep because the time of the last call exceeds the intended period, and the thread will run at 100% time.*

## Semaphores

```c
/** @brief Initialize semaphore handler
 *  @param handler Semaphore handler
 */
void rtosSemaphoreCreate(RtosSemaphore* handler);

/** @brief Obtain the semaphore if available and decrease its counter.
 *  @note The function is used both for binary and counting version of the semaphore.
 *  @param handler A handle to the semaphore being taken
 *  @param timeout The time in ticks to wait for the semaphore to become available
 *  @return
 *   - true: Access to the semaphore has been obtained
 *   - false: The tick limit has expired and the semaphore has not become available
 */
bool rtosSemaphoreTake(RtosSemaphore* handler, RtosTime timeout);

/** @brief Release the semaphore treating it as a binary version.
 *  @param handler A handle to the semaphore to give
 *  @return
 *   - true: The semaphore was released
 *   - false: An error occurred.
 */
bool rtosSemaphoreGiveBinary(RtosSemaphore* handler);

/** @brief Release the semaphore treating it as a counting version.
 *  @param handler A handle to the semaphore to give
 *  @param maxCount Maximum counter value. The value must be greater than 0
 *  @return
 *   - true: The semaphore was released
 *   - false: An error occurred.
 */
bool rtosSemaphoreGiveCounting(RtosSemaphore* handler, uint32_t maxCount);

/** @brief Preview the value of the semaphore.
 *  @note  The binary version operates on the values 0 and 1.
 *  @param handler A handler for the read semaphore
 *  @return The value of the semaphore counter
 */
uint32_t rtosSemaphorePeek(RtosSemaphore* handler);
```

We will also prepare a semaphore API. The ***handler*** is a handle for a semaphore regardless of its type. We have functions to create semaphore, **give** (both in binary and counting versions) and **take**. By **rtosSemaphorePeek**, we can preview the value of the semaphore without modifying it. The binary version has the value 0 and 1, and the counting version is in the range of a 32 bit variable.

## Utilities

```c
/** @brief Return the number of ticks of the system interrupt since the start of the rtosRun().
 *  @note  If an interrupt with a higher priority lasted longer than the time of a single tick,\n
 *         the counter may not show the correct value.
 *  @return System time
 */
RtosTime rtosTicks(void);
```

The function returns system time that has passed since the start of RTOS.

# Application

Before we start implementing anything, we need to prepare a project and a test application. We will try to use the API in an example application.

The project was made in STM32CubeIDE, so you don't need to configure anything after downloading. 

The application code is divided into 3 sections:
- driver - *separates the hardware layer from from the actual application code*.
- code - *contains independent code and the "business" layer*
- rtos - *our RTOS implementation*

![](doc/project_tree.png)

Apart from that, the CubeIDE generated its own paths, which we will not go into.

## Components

The [Application](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/blob/develop/core/code/app/app.cpp) consists of some very simple tasks:

- Watchdog - *driver and code*.
- Status LED - *driver and code*
- Main - *this is the main task*.
- Second - *second task needed to test the semaphore*.

And the IDLE, Tick and Assert handlers.

## Static-analysis

The [CI Pipeline](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/pipelines) was used - Cppcheck and FlawFinder static analysis and Clang-format code formatting. For those who are curious, it is worth having a look at the path [CI](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/tree/develop/ci) and [.gitlab-ci.yml](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/blob/develop/.gitlab-ci.yml) 


## Initialization

### Stack

```c
#define TASK_MAIN_STACK_SIZE_BYTES (64U)
static uint32_t TASK_MAIN_STACK[TASK_MAIN_STACK_SIZE_BYTES];
static RtosTask taskMainHandler;

#define TASK_SECOND_STACK_SIZE_BYTES (64U)
static uint32_t TASK_SECOND_STACK[TASK_SECOND_STACK_SIZE_BYTES];
static RtosTask taskSecondHandler;

#define TASK_WATCHDOG_STACK_SIZE_BYTES (32U)
static uint32_t TASK_WATCHDOG_STACK[TASK_WATCHDOG_STACK_SIZE_BYTES];
static RtosTask taskWatchdogHandler;

#define TASK_LED_STACK_SIZE_BYTES (32U)
static uint32_t TASK_LED_STACK[TASK_LED_STACK_SIZE_BYTES];
static RtosTask taskLedHandler;
```

At the beginning of the file, all handlers, stacks in the form of ordinary arrays, and **#define** are defined. The user can test the operation with dynamically allocated arrays and structures.

### Tasks

```c
/** @brief Ensures proper initialisation of the tasks and runs them.
 *  @note It is the main handler intermediary between an auto-generated c file and c++ code.\n
 *        Prevents the main.c file extension from being automatically changed each time\n
 *        project files are generated in CubeMX.
 */
void app(void)
{
    rtosInit();

    rtosTaskCreate(&taskWatchdogHandler, taskWatchdog, 1, TASK_WATCHDOG_STACK, TASK_WATCHDOG_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskLedHandler, taskLed, 5, TASK_LED_STACK, TASK_LED_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskMainHandler, taskMain, 4, TASK_MAIN_STACK, TASK_MAIN_STACK_SIZE_BYTES);
    rtosTaskCreate(&taskSecondHandler, taskSecond, 3, TASK_SECOND_STACK, TASK_SECOND_STACK_SIZE_BYTES);
    rtosSemaphoreCreate(&semaphoreHandler);
    rtosRun();
}
```

The **app()** function is called in the **main()**, and the whole file is written in C++ to illustrate the compatibility of our RTOS. The user is free to choose priorities, but he must remember about the upper and bottom limits and use one priority per task. This example is based on the LED status task with the highest priority. The Watchdog is the lowest priority task, to show that it is also called, otherwise the system would restart. 

### Semaphore

```c
#define SEMAPHORE_TIMEOUT_TICKS (100U)
static RtosSemaphore semaphoreHandler;
```

Apart from defining a handler for a semaphore, it is also crutial to define a timeout within which the semaphore should be taken.

### Independent tasks

Let's move on to the implementation of the threads, which do not communicate with each other in any way.

```c
static void taskWatchdog(void)
{
    for (;;) {
        watchdogRefresh();
        rtosTaskSleep(1000);
    }
}
```

**taskwatchdog** periodically resets the hardware Watchdog counter and is used to test if the system has crashed. In case of a problem it restarts the application. The task also tests the **rtosTaskSleep** function in the thread with the lowest priority in the system. It is worth trying to change the period or priority of a thread, as well as the load on the entire system in another thread.

```c
static void taskLed(void)
{
    RtosTime lastTime = rtosTicks();

    for (;;) {
        ledOn();
        rtosTaskSleepUntil(&lastTime, 100);
        ledOff();
        rtosTaskSleepUntil(&lastTime, 900);
    }
}
```
**taskLed** is a control task whose effects we will see with the naked eye, because it controls the built-in LED on the BluePill board. The code tests the behavior of the highest priority task and the **rtosTaskSleepUntil** and **rtosTicks** functions. After decrementing the priority of the task and running it afterwards, we may notice some timing problems, but if the delay in accessing the CPU does not exceed 100 ticks, the period of flashing of the LED should not change, because the task will "keep up" by putting the task to sleep for an appropriate time.

### Communication

```c
static void taskMain(void)
{
    rtosTaskSleep(550);

    uint32_t cnt = 0;

    while (1) {
        rtosTaskSleep(10);
        rtosSemaphoreGiveBinary(&semaphoreHandler);
        cnt++;
    }
}
```
The main task waits after the system starts for a certain amount of time and starts giving a binary semaphore with a period of 10 ticks. It is also worth testing the counter version and other delay values, or even no delays. The variable cnt contains the number of given semaphores.

```c
static void taskSecond(void)
{
    uint32_t obtainCnt = 0;
    uint32_t timeoutCnt = 0;

    while (1) {
        if (rtosSemaphoreTake(&semaphoreHandler, SEMAPHORE_TIMEOUT_TICKS)) {
            obtainCnt++;
        }
        else {
            timeoutCnt++;
        }

        rtosTaskSleep(2);
    }
}
```

The **taskSecond** task takes care of semaphore reception, taking into account the number of timeouts and correct operations. For the period of time when the main thread is asleep, there will be a timeout, so the variable **timeoutCnt** should eventually reach 5. After this time, the **rtosSemaphoreTake** function will begin to return a positive result every 10 ticks, with 8 ticks spent inside it and 2 ticks in **rtosTaskSleep(2)** that is at the end of the task.

```mermaid
sequenceDiagram
Note left of Main: Sleep for 550ms
Note over Main,Second: System time: 0ms
Note right of Second: Take attempt 1
Note over Main,Second: System time: 100ms
Note right of Second: Take failed 1
Note right of Second: Sleep for 2ms
Note over Main,Second: ...
Note right of Second: Take attempt 5
Note right of Second: Take failed 5
Note right of Second: Sleep for 2ms
Note over Main,Second: System time: 510ms
Note right of Second: Take attempt 6
Note over Main,Second: System time: 550ms
Note left of Main: Sleep for 10ms
Note over Main,Second: System time: 560ms
Main->Second: Give
Note right of Second: Take succeed 6
Note right of Second: Sleep for 2ms
Note over Main,Second: System time: 562ms
Note right of Second: Take attempt 7
Note over Main,Second: System time: 570ms
Main->Second: Give
Note right of Second: Take succeed 7
Note right of Second: Sleep for 2ms
Note over Main,Second: ...
```

## Hooks

The user application uses all the extra options from the API. Note the repeated **extern "C "** needed because of our source file in C++.

### Idle

```c
extern "C" void rtosIdleHook(void)
{
    __WFI();
}
```

The IDLE task function is defined, which context is activated in case of spare CPU time in the system (while all other tasks are asleep). In our case, the core is put to sleep until the next interrupt happens, which is when the context is switched. This approach is very cost-effective in low-power applications. 

### Tick

```c
extern "C" void rtosTickHook(void)
{
    static volatile uint32_t cnt = 0;
    cnt++;
}
```

When a cyclic system interrupt occurs, the **rtosTickHook** function is called, in which, for example, a static variable is incremented. One should remember about the fact that the function affects the system's performance, because it is called relatively often and uses a separate stack.

## Assertion

```c
extern "C" void rtosAssert(bool condition)
{
    if (!condition) {
        __disable_irq();  // Disable interrupts to prevent context switch

        if (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) {
            __asm("BKPT #0");  // Insert break when in debug mode
        }

        CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk;  // Disable TRC
        CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;   // Enable TRC
        DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;             // Disable clock cycle counter
        DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;              // Enable clock cycle counter
        DWT->CYCCNT = 0;                                  // Reset cycle counter

        for (;;) {
            ledToggle();

            uint32_t cycles = (SystemCoreClock / 1000000U) * 1000U * 100U;  // 100ms period
            for (uint32_t start = DWT->CYCCNT; (DWT->CYCCNT - start) < cycles;) {
            }
        }
    }
}
```

The assert function needs a thorough explanation. The **condition** argument specifies whether the test expression is true. If it is not, the code is executed, trying to safely stop the system. This is the only place in the code where no driver has been provided to separate the application code from the hardware.

1. At the very beginning, the interrupts are switched off globally to prevent the problem from propagating.
2. In the next step, we test the bit responsible for detecting whether the Debug session is active. If so, then we perform a hardware breakpoint, which results in immediately stopping the core and moving the user to the place in the code where the problem occurred. This ends the operation. 
3. Otherwise, we have no way of detecting if something has happened. So we need some way to animate this with an LED. Tasks do not work and we have all the interrupts disabled, so we are left with something like executing a sequence of instructions that lasts for a period of time simulating a delay. If we wanted to do it in a classic loop, we would be depended on the core clock and compiler optimizations. This doesn't sound like a good idea for maintaining a rigid time frame. Fortunately, the Cortex-M3/M4 has a dedicated counter for debugging purposes, with which we can easily count down the time in microseconds. So we initialize the module and go into an infinite loop.
4. In this loop, we change the state of the LED every 100ms and wait in polling mode for the time to elapse by looking into the CYCCNT register in DWT.

# Source code

Let's talk CODE, which is what we've all been waiting for.

## Dependency

```c
#include "rtos.h"
#include "main.h"
```

We include our RTOS header file along with the [main.h](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/blob/develop/Inc/main.h) that the user should add. This file should also include the [CMSIS](https://developer.arm.com/tools-and-software/embedded/cmsis) libraries depending on the user's vision. In this example [HAL](https://www.st.com/resource/en/user_manual/dm00154093-description-of-stm32f1-hal-and-lowlayer-drivers-stmicroelectronics.pdf) will be used containing CMSIS libraries.

## Macros

To introduce a convenient way to configure the IDLE, Tick, and Assert hooks mentioned earlier in the API, appropriate macros were used.

```c
#if (RTOS_USE_ASSERT > 0)
extern void rtosAssert(bool condition);
#define RTOS_ASSERT(x) rtosAssert(x)
#else
#define RTOS_ASSERT(x)
#endif
```

For the purpose of testing the RTOS and using its API, an assert option will be prepared. For 2 configuration cases, a macro is defined in the API to paste a function call or skip it in order to optimize the execution time and memory usage of the program. We have already defined **rtosAssert** function earlier, so there will be no problem with linking stage.

```c
#if (RTOS_USE_IDLE_HOOK > 0)
extern void rtosIdleHook(void);
#define RTOS_IDLE_HOOK() rtosIdleHook()
#else
#define RTOS_IDLE_HOOK()
#endif
```

A classic IDLE task is nothing more than a function containing a loop that doesn't do anything productive or, in a more complex system, deals with resources that are slowing it down, memory deallocation, or even defragmentation. An approach that takes advantage of currently free CPU time would be to call the **rtosIdleHook** function in an IDLE thread, giving the user a simple way to write a few instructions or create their own loop that mimics the original IDLE thread.

```c
#if (RTOS_USE_TICK_HOOK > 0)
extern void rtosTickHook(void);
#define RTOS_TICK_HOOK() rtosTickHook()
#else
#define RTOS_TICK_HOOK()
#endif
```

The Tick function works on a similar principle, with the difference being that we cannot define a function with an internal loop, because this function is to be called with every occurrence of the system tick in separation from any thread. Of course, we can omit it, which will reduce the number of unnecessary instructions to execute.

```c
#define RTOS_MAX_PRIORITY (32U)
```

At the end, one more simple macro that is responsible for storing the maximum number of supported threads/priorities. We will use it quite often, so it is better not to hardcode it multiple times.

## Scheduling - theory

No more assumptions and obviousness. You're probably wondering by now, how the actual task planner code works. What ideas do we have about the operating principle? What can we use in Cortex-M3? You will see in a moment that it's not a black magic and it's not at all that difficult to implement either.

### How to switch context?

What is context switching anyway? It is a process (set of instructions) that results in a clever swap of CPU registers, so that in the next cycle a completely different code is executed, along with access to the last stored data set and the "position" in the code itself.

During the normal operation of the program, subsequent assembler instructions are executed. We make jumps to subroutines, put something on and off the stack, store data in registers, or read the results of status registers. The CPU is a simple logic machine, simply executing sequential opcodes. It is simply the human thought written in assembler that defines the behavior of the program. For this reason, we can intelligently streamline our work by creating a mechanism that swaps the set of registers to create contexts.

Cortex-M3 has the following set of registers:

![](doc/registers.png)

R0-R12 are 32-bit general-purpose registers for data operations.

R13 is the stack pointer (SP). In the Cortex-M3 processor, there are two stack pointers. This duality allows two separate stack memories to be set up. When using the register name R13, you can only access the current SP. The other one is inaccessible unless one uses special instructions to move to special register from general-purpose register (MSR) and move special register to general-purpose register (MRS). The two SPs are as follows:

Main Stack Pointer (MSP) or SP_main in ARM documentation:
This is the default SP and is used by the operating system (OS) kernel, exception handlers, and all application codes that require privileged access.

Process Stack Pointer (PSP) or SP_ process in ARM documentation:
This is used by the base-level application code (while not running an exception handler).

It is not necessary to use both SPs. Simple applications can rely purely on the MSP. The SPs are used for accessing stack memory processes such as PUSH and POP.

**Link Register**
The Link Register (LR) is register R14. It stores the return information for subroutines, function calls, and exceptions. On reset, the processor sets the LR value to `0xFFFFFFFF`. This register can be written to control the program flow.

**Program Counter**
The Program Counter (PC) is register R15. It contains the current program address. On reset, the processor loads the PC with the value of the reset vector, which is at address `0x00000004`. Bit[0] of the value is loaded into the EPSR T-bit at reset and must be 1. This register can be written to control the program flow.

**Program Status Register**
The Cortex-M3 processor also has a number of special registers. Program Status registers (PSRs) Interrupt Mask registers (PRIMASK, FAULTMASK,and BASEPRI) Control register (CONTROL). These registers have special functions and can be accessed only by special instructions. They cannot be used for normal data processing.

The Program Status Register (PSR) combines:
- Application Program Status Register (APSR)
- Interrupt Program Status Register (IPSR)
- Execution Program Status Register (EPSR).

These registers are mutually exclusive bitfields in the 32-bit PSR. The bit assignments are:

![](doc/status_register.png)

It seems that we need to implement a routine that would modify these registers.

### PendSV and SysTick

How do we interrupt the CPU to call our additional code?
Interrupt? - I have heard that before... 
We use hardware interrupts on microcontrollers all the time. Why not use them this time?

The Cortex-M3 designers have provided hardware support for such a solution and prepared several system interrupts. Today we will focus on Systick and PendSV.

Control of STM32 tasks is greatly facilitated by three system exceptions that are available in the Cortex architecture. Their target task is to work under the control of the operating system, although in applications without an OS, they can also be used with great effect to provide greater control and stability of operation.

For cyclic task context switching, there is a system 24-bit SysTick timer ready to be used. Its task is to generate an interrupt at specified intervals, and its handler function can take care of task context switching.

In the simplest operating system, a SysTick timer is responsible for switching contexts of running tasks. A simple but not always obvious problem may arise during the operation of such a system.

During execution of a task (user program), an interrupt may be reported, which will preempt the current process. If, while handling the reported interrupt, the SysTick timer interrupts it and the OS starts switching task contexts, then by exiting the interrupt handling function from the SysTick timer, the OS will try to force the microcontroller to start a new task. This is obviously erroneous behavior because the handling of the first interrupt will be significantly delayed.

The solution to this problem is to use the PendSV exception. Its programmable priority is set to the lowest possible level so that this interrupt will never preempt other interrupts being handled.

Let us now analyze the behavior of the system with PendSV support implemented. Assume that a task running on the system currently has nothing to do. It generates an task context switching and call the PendSV interrupt. Only this last interrupt performs a proper context switching so that when the microcontroller returns to normal program execution, the next task is already being executed.

If during the task context switching in the PendSV interrupt handler function, the system registers another interrupt, the context switching is halted by expropriating PendSV (remember that its priority is the lowest).

![](doc/user_request.png)

The same is true when the operating system prepares for task context switching using an interrupt from the SysTick timer. In such a situation, assuming that the SysTick interrupt has a high priority and some other exception was handled moments before, the latter will be preempted in favor of SysTick.

![](doc/cyclic.png)

Since handling the preempted interrupt is far more important than executing the tasks running on the system, the SysTick timer interrupt handler function prepares the system to switch task contexts and generates the PendSV exception. Now, since PendSV has the lowest priority, the microcontroller returns to handling the previously preempted interrupt. When the interrupt handling activities are complete, the pending PendSV exception begins to be handled, the consequence of which is to switch the context and begin handling the next task running in the system.

### Time complexity

Let's pay attention to code optimization at the very beginning of its writing. Usually pre-optimization is a source of problems early on, but in this case we are dealing with the foundation of future applications. For this reason, it is worth considering how to use resources wisely.

The first thing that comes to mind is to create an empty array with consecutive tasks and add them one by one with their priority. When switching contexts, we would search the entire array for the task with the highest priority that is ready to run. In this way, we would achieve simplicity of implementation, but is it an optimal solution? It turns out that it is not and there are many different approaches to the performance problem. Here we will present one of them. 

The simple solution has 2 disadvantages:
- linear time to search the thread list
- excessive use of RAM

What do you think about optimizing these 2 parameters? 

Instead of a list of tasks, i.e. whole structures with data, we could create a smaller array that would also store the relevant information, and leave the rest to the user. This can be implemented by creating an array of pointers to the tasks, initially initialized with `NULL` values. The user would add more tasks by creating appropriate structures with the **rtosTaskCreate** function, which would be added to the array. This way, for a small number of tasks in the system, we do not waste RAM resources.

How to deal with the optimization of searching the array? There is nothing for free. One of the options that we will use is to limit the functionality of the RTOS in terms of possible task priorities. Imagine the following concept:
- in an additional array (of bits), we'll write to ourselves whether the task with a given index is ready to run
- consider that the index in the array also determines the priority of the task, i.e. each task has exclusive right to a given priority which cannot be repeated
- if a task with a given priority is ready to run, we can determine its priority by simply counting how many zeros have appeared since the oldest position in the array
- due to the limitation of 32 supported threads, we can significantly optimize such an array by simply using a 4-bit variable to which we have quick access
- the highest priority means the oldest (leftmost) bit

Isn't that simple? Additionally, we have a ready-to-use assembler instruction that counts the number of significant zeros in the 4B variable. Its wrapped equivalent in CMSIS is **__builtin_clz**.

## Scheduling - the actual code

Now that the concept is ready, we can move on to the implementation in [rtos.c](https://gitlab.com/BloedeBleidd/cortex-m3-rtos-tutorial/-/blob/develop/core/rtos/rtos.c)

### Task creation

```c
void rtosTaskCreate(RtosTask* handler, void (*task)(void), RtosPriority priority, void* stack, uint32_t stackSize)
{
    RTOS_ASSERT(NULL != handler);
    RTOS_ASSERT(NULL != task);
    RTOS_ASSERT(32U >= priority);
    RTOS_ASSERT(NULL != stack);
    RTOS_ASSERT(stackSize);
    uint32_t* sp = (uint32_t*)((((uint32_t)(stack) + stackSize * sizeof(uint32_t)) / 8U) * 8U);  // Double word align
    *(--sp) = 1U << 24U;       // xPSR with Thumb bit enabled
    *(--sp) = (uint32_t)task;  // PC
    sp -= 14U;                 // LR, R12, R3-R0, R11-R4
    handler->stackPointer = sp;
    handler->priority = priority;
    thread[priority] = handler;

    if (priority) {
        ready |= (1U << (priority - 1U));
    }
}
```

Adding a new task involves populating the TCB structure and updating static variables.
- At the very beginning, we test the ranges of parameters given by the user.
- We align the stack address assigned by the user to the double-word, which is 8 bytes (variable **sp**). This is a requirement related to the Cortex-M3 architecture. The stack grows towards smaller addresses, so we align from the end address.
- We must save the initial context of the thread registers. It is worth remembering the order of registers because we will have to write them on the stack in reverse order.
- In the status register, we activate the flag indicating the use of Thumb commands and move the address
- In the Program Counter we write the address of the user function and move the address
- We move 14 addresses, which consist of registers **LR, R12, R3-R0, R11-R4** in exactly that order. These values can be filled with zero, but in our code it was omitted for optimization purposes.
- We update the TCB structure.
- We add a pointer to the thread list.
- We set the appropriate position in the variable that holds the bitwise priorities.

### Initialization

```c
void rtosInit(void)
{
    rtosTaskCreate(&idleThread, idleThreadFunc, 0U, idleStack, RTOS_IDLE_STACK_SIZE_WORDS);
}
```

There must be an IDLE task in the system, so the first function to be called is the RTOS initializer. The only action to be performed is to create a task with zero priority.

### Idle task

```c
static void idleThreadFunc(void)
{
    while (1) {
        RTOS_IDLE_HOOK();
    }
}
```
This task does not require a proper place in our priority variable because task IDLE is always ready to be executed. If the CLZ command returns a value of 32, i.e., all bits are reset to zero, the IDLE task is called.

The task calls the user's hook, if such an option was selected.

### Task switching

```c
static volatile RtosTime systemTime;  //!< System tick counter
static RtosTask* volatile current;    //!< Currently executing thread handler
static RtosTask* volatile next;       //!< Next thread to be executed
static RtosTask* thread[RTOS_MAX_PRIORITY + 1U];  //!< List of tasks, taking into account priorities. The idle task is the first element.
static uint32_t ready;       //!< A set of flags indicating whether the thread is ready to run
static uint32_t delayed;     //!< A set of flags indicating whether the thread should sleep
static RtosTask idleThread;  //!< Idle task handler
static uint32_t idleStack[RTOS_IDLE_STACK_SIZE_WORDS];  //!< Idle task stack
```

In the listing above, we can see a set of static variables defined in the system. There are 35 pointers to tasks in total, two 4-byte long variables, and data about the IDLE task. The structure and the stack of the IDLE task are reserved locally, not allowing the user to do so. The meaning of the **delayed** variable will be explained later.

```c
void rtosRun(void)
{
    NVIC_SetPriority(PendSV_IRQn, 0xffU);
    SysTick_Config(SystemCoreClock / RTOS_TICKS_PER_SECOND);
    NVIC_SetPriority(SysTick_IRQn, 0U);
    __disable_irq();
    schedule();
    __enable_irq();
}
```

The function that starts the RTOS operation is **rtosRun**. Through the NVIC module, we give low priority to the PendSV interrupt, where the actual context change will take place according to the principle explained in the theoretical introduction. We use SysTick to update the system time.

To calculate the counter value, we will use a user-defined external variable.

```c
extern uint32_t SystemCoreClock;  //!< CPU speed in Hz
```

We set the SysTick interrupt to the highest priority so that the tick is not missed by excessive delays to other interrupts in the system.

We then globally disable interrupts to prevent unexpected preemptions of the SysTick interrupt. We invoke the task scheduler routine (more on that in a moment) and restart the interrupts, allowing the PendSV routine to start.

```c
static void schedule(void)
{
    if (ready) {
        next = *(thread + RTOS_MAX_PRIORITY - __builtin_clz(ready));
    }
    else {
        next = *thread;
    }

    if (next != current) {
        SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
    }
}
```

The routine contained in the **schedule** function is reused in several places, so it has been separated.
- At the beginning, we check if there are any pending tasks to be handled, which is indicated by a value other than zero in the **ready** variable.
- If such a task exists, its position in the pointer array is calculated and rewritten to the variable **next**, which is the next task to be handled.
- If not, we will have to call the task IDLE.
- We check if the currently running task is the same one that we have just selected. If so, we do not need to switch contexts, which would be a waste of CPU time.
- If the pointers are different, we signal in the System control block register that PendSV should be interrupted. *A similar bit is set in hardware for any hardware interrupt in the system, such as the end of an SPI transmission*.

So every time we need to switch contexts, we will use the **schedule** function, which will result in calling PendSV with the assembler routine in the near future.

### Assembler

```c=
__attribute__((naked)) void PendSV_Handler(void)
{
    __asm(
        " CPSID i           \n"  // __disable_irq();
        " LDR   r3,=current \n"  // if (current) {
        " LDR   r2,[r3,#0]  \n"
        " CBZ   r2,restore  \n"
        " PUSH  {r4-r11}    \n"  // push registers
        " LDR   r2,[r3,#0]  \n"  // current->stackPointer = sp;
        " STR.W sp,[r2]     \n"
        " restore:          \n"  // }
        " LDR   r2,=next    \n"  // sp = next->stackPointer;
        " LDR   r1,[r2,#0]  \n"
        " LDR   sp,[r1,#0]  \n"
        " LDR   r2,[r2,#0]  \n"  // current = next;
        " STR   r2,[r3,#0]  \n"
        " POP   {r4-r11}    \n"  // pop registers
        " CPSIE i           \n"  // __enable_irq();
        " BX    lr          \n");
}
```
We define the **PendSV_Handler** interrupt function with the **naked** attribute, which in GCC specifies that the function body will be written in pure assembler. The compiler will not add its code.

4. In the first step **CPSID i** disables interrupts globally, because we don't want anything to interrupt the execution of our key RTOS code.
5. We load the pointer stored in the **current** variable into the R3 register. This is the most recently executed thread that has just been stopped.
6. We store the value that R3 was pointing to in register R2. There is nothing more than the stack of the last executed task.
7. With instruction **CBZ** we test if it is zero. If yes, we skip to line 12.
8. Before changing threads, throw registers R4-R11 onto the current stack, remembering the context.
9. The stack pointer is loaded into R2.
10. Into the **current** structure at the stack position we write the stack pointer, i.e. itself.
11. Header for **CBZ** instruction.
12. To R2 we write the stack pointer of the next task to be executed.
13. To R1 we write the stack address of the **next** task.
14. Update the stack with the new value from R1.
15. The next two instructions copy the **next** pointer to **current**.
17. We restore the stack, but this time it is the stack of the new task.
18. We activate the interrupts before exiting the PendSV routine.
19. Return from the PendSV function.

### Cyclic interrupt

```c
void SysTick_Handler(void)
{
    systemTime++;
    RTOS_TICK_HOOK();

    for (uint32_t working = delayed; working;) {
        RtosTask* task = *(thread + RTOS_MAX_PRIORITY - __builtin_clz(working));
        uint32_t bit = (1U << (task->priority - 1U));
        task->timeout--;

        if (!(task->timeout)) {
            ready |= bit;
            delayed &= ~bit;
        }

        working &= ~bit;
    }

    __disable_irq();
    schedule();
    __enable_irq();
}
```

We already know how context switching occurs. Now we need to run a mechanism that will automatically plan the selection of the appropriate task in the optimal way that we came up with.
- At the beginning, the action associated with updating the variable that tracks the system time and the already known user hook call.
- In the loop, we perform actions on a variable not described before, namely **delayed**. The concept of this variable is similar to **ready** to contain information about the next tasks in the system. This time, however, it is information whether the task is dormant. The difference between **ready** and **delayed** is that if there are 2 threads in the system that are not asleep, only one of them will get CPU time.
- The **working** variable is a temporary copy of **delayed**.
- One by one, we go through all available tasks and decrease the **timeout**, which is stored in the task's structure and describes how long the task should remain dormant. The **timeout** does not go through all 32 tasks, only the dormant ones, which improves the performance of the algorithm.
- If the timeout has passed, we consider the task ready for execution and it will be taken into account during the next call of **schedule**.
- At the very end, we call the scheduler, which will result in a call to PendSV as well.

### Sleep

```c
void rtosTaskSleep(RtosTime ticks)
{
    __disable_irq();
    uint32_t bit = (1U << (current->priority - 1U));
    ready &= ~bit;
    delayed |= bit;
    current->timeout = ticks;
    schedule();
    __enable_irq();
}
```

We talked about putting threads to sleep, so let's move on to the sleep function **rtosTaskSleep**. To store the number of ticks to sleep, we need to write this value to TCB. We momentarily stop the interrupts to safely change the variables. We calculate the position in **ready** and **delayed** based on the **current** pointer, which points precisely to our currently running task. We clear the bit in **ready** because a dormant task should definitely not be considered for the next scheduling. Set the bit in **delayed** and save the **timeout**. After changing all the variables, we can execute the scheduling algorithm because we are sure about the change because the current one went to sleep. **PendSV** will select a new task as soon as the interrupts start.

```c
void rtosTaskSleepUntil(RtosTime* const lastTick, RtosTime ticks)
{
    RTOS_ASSERT(NULL != lastTick);
    __disable_irq();
    const RtosTime systemTicks = systemTime;
    const RtosTime tickToWake = *lastTick + ticks;
    bool toSleep = false;

    if (systemTicks < *lastTick) {
        if ((tickToWake < *lastTick) && (tickToWake > systemTicks)) {
            toSleep = true;
        }
    }
    else {
        if ((tickToWake < *lastTick) || (tickToWake > systemTicks)) {
            toSleep = true;
        }
    }

    *lastTick = tickToWake;

    if (toSleep) {
        uint32_t bit = (1U << (current->priority - 1U));
        ready &= ~bit;
        delayed |= bit;
        current->timeout = ticks;
        schedule();
    }

    __enable_irq();
}
```
A more sophisticated version of sleep is **rtosTaskSleepUntil**, which takes variable sleep time into account. The concept is explained at the beginning of the tutorial. Please study the code yourself.

## Semaphore

Synchronization between different threads or cores has always been a complex task because it requires special logic. If two competing threads modify the same common variable, it may come to a situation when one of them reads the value and subsequently caching it in a register for later modification in order to optimize it. If the second thread with higher priority starts to perform the same scenario, then there will be a situation when two consecutive writes will occur, resulting in an incorrect value of the variable and probably a system crash.

The only solution to this problem is exclusive access to memory. In other words, only one thread should have access to shared memory at a time until it completes its RMW Read-Modify-Write operation (or the write operation must be aborted if a higher priority thread has gained access before the cycle ends).

### Binary and counting semaphore

We have 2 types of semaphores to implement, i.e., counting and binary. Let's see if this time the wise heads of designers at ARM have not prepared some ready-made solution built into the architecture.

### Cortex-M3 - Synchronization primitives

As it turns out, there is a certain mechanism referred to as - [Synchronization primitives](https://developer.arm.com/documentation/dui0552/a/the-cortex-m3-processor/memory-model/synchronization-primitives)

On ARMv7-M and ARMv7E-M (Cortex-M3/M4/M7) architectures, there is an exclusive monitor mechanism that exactly solves RMW problems. We have some useful operations called "Load-Exclusive" (LDREX) and "Store-Exclusive" (STREX). Conceptually, the LDREX operation performs a read of a value, and also sets up a special module to observe whether the variable being loaded can be written to by something else. Executing STREX for an address used by the last LDREX will only write that address if no one else wrote it first. The STREX instruction will return 0 if the write took place, or 1 if it was aborted.

There are many situations in which STREX may choose not to write a value, even if the location has not actually been violated. For example, an interrupt between LDREX and STREX will cause STREX to assume that the observed location may have been overwritten. For this reason, it is usually a good idea to minimize the amount of code between LDREX and STREX, and preferably to disable interrupts.

### Implementation

```c
void rtosSemaphoreCreate(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    *handler = 0U;
}
```

We assume initial value for the semaphore is 0.

```c
bool rtosSemaphoreTake(RtosSemaphore* handler, RtosTime timeout)
{
    RTOS_ASSERT(NULL != handler);

    do {
        uint32_t tmp = __LDREXW(handler);

        if (tmp) {
            tmp--;
            __DMB();

            if (!(__STREXW(tmp, handler))) {
                __DMB();
                return true;
            }
        }

        if (timeout) {
            timeout--;
            rtosTaskSleep(1U);
        }
    } while (timeout);

    return false;
}
```

To take a semaphore, we need to reduce the semaphore's value, assuming that it is possible at the given moment. The scenario works for the binary version as well as the count version.

In the loop, we test if the semaphore value taken by **__LDREXW** is positive, then we decrement it and try to write it back. If it is positive, we terminate the operation and exit the function. But if the write fails, it means we have to try for a certain **timeout**, so the **timeout** variable is decremented locally and the thread is put to sleep for a single system tick. The attempt is made until a positive result or timeout is reached.

Note the [**__DMB**](https://developer.arm.com/documentation/dui0552/a/the-cortex-m3-instruction-set/miscellaneous-instructions/dmb) instruction, which ensures that all explicit memory accesses that occur in the program are preserved in exact order.

```c
bool rtosSemaphoreGiveCounting(RtosSemaphore* handler, uint32_t maxCount)
{
    RTOS_ASSERT(NULL != handler);
    RTOS_ASSERT(1U <= maxCount);
    uint32_t tmp = __LDREXW(handler);

    if (tmp < maxCount) {
        tmp++;
        __DMB();

        if (!(__STREXW(tmp, handler))) {
            __DMB();
            return true;
        }
    }

    return false;
}
```

Giving back a semaphore is almost the same operation, only this time we increment the variable and check whether it exceeds the specified limit. The operation of returning a semaphore does not need a timeout, hence the lack of a loop, and the attempt is made once.

```c
bool rtosSemaphoreGiveBinary(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    return rtosSemaphoreGiveCounting(handler, 1U);
}
```

Binary version uses a counting implementation where the limit is set to 1.

```c
uint32_t rtosSemaphorePeek(RtosSemaphore* handler)
{
    RTOS_ASSERT(NULL != handler);
    return __LDREXW(handler);
}
```

As an add-on, we create a function to preview the value of the semaphore, i.e., a simple read of a variable.

## Optimization

```c
#pragma GCC push_options
#pragma GCC optimize("-Ofast")

...code

#pragma GCC pop_options
```

All RTOS code is optimized in terms of speed of execution regardless of the global choice of flags in the project. While writing our RTOS we were careful to prevent the compiler from optimizing our code in many places to protect ourselves in case of excessive optimization. We can now enjoy it.

# Measurements

To test the performance of our RTOS, we can measure or calculate the number of instructions to execute for several configurations.

## Context switching time

The context switching time is nothing but the time that elapses after we call the sleep function or a SysTick interrupt occurs by itself.

### User request

A context switch can occur after a thread goes to sleep at any time not associated with a system tick. The number of instructions to be executed can be estimated from the assembly code. 

For the **rtosTaskSleep** function, this is the delay associated with the instruction set:

```asm
080013b8 <rtosTaskSleep>:
 80013b8:	b470      	push	{r4, r5, r6}
 80013ba:	b672      	cpsid	i
 80013bc:	2201      	movs	r2, #1
 80013be:	4916      	ldr	r1, [pc, #88]	; (8001418 <rtosTaskSleep+0x60>)
 80013c0:	4d16      	ldr	r5, [pc, #88]	; (800141c <rtosTaskSleep+0x64>)
 80013c2:	680b      	ldr	r3, [r1, #0]
 80013c4:	4c16      	ldr	r4, [pc, #88]	; (8001420 <rtosTaskSleep+0x68>)
 80013c6:	f893 c008 	ldrb.w	ip, [r3, #8]
 80013ca:	680e      	ldr	r6, [r1, #0]
 80013cc:	682b      	ldr	r3, [r5, #0]
 80013ce:	f10c 3cff 	add.w	ip, ip, #4294967295	; 0xffffffff
 80013d2:	6070      	str	r0, [r6, #4]
 80013d4:	6820      	ldr	r0, [r4, #0]
 80013d6:	fa02 f20c 	lsl.w	r2, r2, ip
 80013da:	ea23 0302 	bic.w	r3, r3, r2
 80013de:	4302      	orrs	r2, r0
 80013e0:	602b      	str	r3, [r5, #0]
 80013e2:	6022      	str	r2, [r4, #0]
 80013e4:	b19b      	cbz	r3, 800140e <rtosTaskSleep+0x56>
 80013e6:	fab3 f383 	clz	r3, r3
 80013ea:	480e      	ldr	r0, [pc, #56]	; (8001424 <rtosTaskSleep+0x6c>)
 80013ec:	f1c3 0320 	rsb	r3, r3, #32
 80013f0:	4a0d      	ldr	r2, [pc, #52]	; (8001428 <rtosTaskSleep+0x70>)
 80013f2:	f850 3023 	ldr.w	r3, [r0, r3, lsl #2]
 80013f6:	6013      	str	r3, [r2, #0]
 80013f8:	6812      	ldr	r2, [r2, #0]
 80013fa:	680b      	ldr	r3, [r1, #0]
 80013fc:	429a      	cmp	r2, r3
 80013fe:	bf1e      	ittt	ne
 8001400:	f04f 5280 	movne.w	r2, #268435456	; 0x10000000
 8001404:	4b09      	ldrne	r3, [pc, #36]	; (800142c <rtosTaskSleep+0x74>)
 8001406:	605a      	strne	r2, [r3, #4]
 8001408:	b662      	cpsie	i
 800140a:	bc70      	pop	{r4, r5, r6}
 800140c:	4770      	bx	lr
 800140e:	4b05      	ldr	r3, [pc, #20]	; (8001424 <rtosTaskSleep+0x6c>)
 8001410:	4a05      	ldr	r2, [pc, #20]	; (8001428 <rtosTaskSleep+0x70>)
 8001412:	681b      	ldr	r3, [r3, #0]
 8001414:	e7ef      	b.n	80013f6 <rtosTaskSleep+0x3e>
 8001416:	bf00      	nop
```

As we can see, our **schedule** function was optimized, and even removed from the assembly listing despite the lack of **inline**.

When the interrupts are enabled in the last instruction, there is a jump to **PendSV**, which is a literal copy of our assembler code:

```asm
0800166c <PendSV_Handler>:
 800166c:	b672      	cpsid	i
 800166e:	4b09      	ldr	r3, [pc, #36]	; (8001694 <restore+0x16>)
 8001670:	681a      	ldr	r2, [r3, #0]
 8001672:	b122      	cbz	r2, 800167e <restore>
 8001674:	e92d 0ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
 8001678:	681a      	ldr	r2, [r3, #0]
 800167a:	f8c2 d000 	str.w	sp, [r2]

0800167e <restore>:
 800167e:	4a06      	ldr	r2, [pc, #24]	; (8001698 <restore+0x1a>)
 8001680:	6811      	ldr	r1, [r2, #0]
 8001682:	f8d1 d000 	ldr.w	sp, [r1]
 8001686:	6812      	ldr	r2, [r2, #0]
 8001688:	601a      	str	r2, [r3, #0]
 800168a:	e8bd 0ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
 800168e:	b662      	cpsie	i
 8001690:	4770      	bx	lr
```

These are the only lines of code that will be called when switching contexts on a user's request.

### Cyclic schedululing

If the planner is called cyclically, the first part is replaced by the **SysTick** interrupt handler:

```asm
080015c4 <SysTick_Handler>:
 80015c4:	4a22      	ldr	r2, [pc, #136]	; (8001650 <SysTick_Handler+0x8c>)
 80015c6:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 80015c8:	6813      	ldr	r3, [r2, #0]
 80015ca:	3301      	adds	r3, #1
 80015cc:	6013      	str	r3, [r2, #0]
 80015ce:	f7ff fdc9 	bl	8001164 <rtosTickHook>
 80015d2:	f8df e080 	ldr.w	lr, [pc, #128]	; 8001654 <SysTick_Handler+0x90>
 80015d6:	f8de 1000 	ldr.w	r1, [lr]
 80015da:	2900      	cmp	r1, #0
 80015dc:	d034      	beq.n	8001648 <SysTick_Handler+0x84>
 80015de:	460d      	mov	r5, r1
 80015e0:	f04f 0c01 	mov.w	ip, #1
 80015e4:	4f1c      	ldr	r7, [pc, #112]	; (8001658 <SysTick_Handler+0x94>)
 80015e6:	4c1d      	ldr	r4, [pc, #116]	; (800165c <SysTick_Handler+0x98>)
 80015e8:	683e      	ldr	r6, [r7, #0]
 80015ea:	fab1 f381 	clz	r3, r1
 80015ee:	f1c3 0320 	rsb	r3, r3, #32
 80015f2:	f854 0023 	ldr.w	r0, [r4, r3, lsl #2]
 80015f6:	6842      	ldr	r2, [r0, #4]
 80015f8:	7a03      	ldrb	r3, [r0, #8]
 80015fa:	3a01      	subs	r2, #1
 80015fc:	3b01      	subs	r3, #1
 80015fe:	fa0c f303 	lsl.w	r3, ip, r3
 8001602:	6042      	str	r2, [r0, #4]
 8001604:	b912      	cbnz	r2, 800160c <SysTick_Handler+0x48>
 8001606:	431e      	orrs	r6, r3
 8001608:	ea25 0503 	bic.w	r5, r5, r3
 800160c:	4399      	bics	r1, r3
 800160e:	d1ec      	bne.n	80015ea <SysTick_Handler+0x26>
 8001610:	603e      	str	r6, [r7, #0]
 8001612:	f8ce 5000 	str.w	r5, [lr]
 8001616:	b672      	cpsid	i
 8001618:	683b      	ldr	r3, [r7, #0]
 800161a:	b193      	cbz	r3, 8001642 <SysTick_Handler+0x7e>
 800161c:	fab3 f383 	clz	r3, r3
 8001620:	f1c3 0320 	rsb	r3, r3, #32
 8001624:	4a0e      	ldr	r2, [pc, #56]	; (8001660 <SysTick_Handler+0x9c>)
 8001626:	f854 3023 	ldr.w	r3, [r4, r3, lsl #2]
 800162a:	6013      	str	r3, [r2, #0]
 800162c:	4b0d      	ldr	r3, [pc, #52]	; (8001664 <SysTick_Handler+0xa0>)
 800162e:	6812      	ldr	r2, [r2, #0]
 8001630:	681b      	ldr	r3, [r3, #0]
 8001632:	429a      	cmp	r2, r3
 8001634:	bf1e      	ittt	ne
 8001636:	f04f 5280 	movne.w	r2, #268435456	; 0x10000000
 800163a:	4b0b      	ldrne	r3, [pc, #44]	; (8001668 <SysTick_Handler+0xa4>)
 800163c:	605a      	strne	r2, [r3, #4]
 800163e:	b662      	cpsie	i
 8001640:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
 8001642:	6823      	ldr	r3, [r4, #0]
 8001644:	4a06      	ldr	r2, [pc, #24]	; (8001660 <SysTick_Handler+0x9c>)
 8001646:	e7f0      	b.n	800162a <SysTick_Handler+0x66>
 8001648:	4f03      	ldr	r7, [pc, #12]	; (8001658 <SysTick_Handler+0x94>)
 800164a:	4c04      	ldr	r4, [pc, #16]	; (800165c <SysTick_Handler+0x98>)
 800164c:	e7e3      	b.n	8001616 <SysTick_Handler+0x52>
 800164e:	bf00      	nop
```

## CPU overhead

CPU overhead depends linearly on the **SysTick** frequency and the number of requests from the user. It was possible to use the concept presented at the beginning, which resulted in moving from linear CPU time complexity to logarithmic (**CLZ** instruction). The sheer amount of code that needs to be executed relative to large RTOSes is several times smaller.

Those curious can use the approach of changing the GPIO state at key moments in the code to see for themselves on a oscilloscope. Have fun ;)

## Memory usage

The image shows the RAM and FLASH resources used by the compiled RTOS module:

![](doc/Ofast.png)<br>
Optimization -Ofast 

![](doc/Os.png)<br>
Optimization -Os

![](doc/Os_small.png)<br>
Optimization -Os, without hooks and assertion

Depending on the expected result, you can choose which version suits you better. If you don't need advanced RTOS features, but want speed, power efficiency, and low memory usage, consider writing your own RTOS. Solutions such as FreeRTOS are [documented in this regard](https://www.freertos.org/FAQMem.html#RAMUse).

In our case, the size of the semaphore is contained in a 4 byte variable and access to a few instructions, while in FreeRTOS we are dealing with 76 bytes to start and hundreds of instructions to execute.

The case of TCB structures is similar. In our case, it is 9 bytes plus stack, while in FreeRTOS it is 64 bytes + stack, not to mention the number of instructions to be executed and the response time itself.

###### tags: `RTOS` `Cortex-M3` `ARMv7-M` `Semaphore`
