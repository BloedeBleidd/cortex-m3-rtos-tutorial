#!/bin/bash

echo "formatting code folder..."
find core/code -name "*.h" -o -name "*.c" | xargs clang-format -i -style=file -fallback-style=none

echo "formatting driver folder..."
find core/driver -name "*.h" -o -name "*.c" | xargs clang-format -i -style=file -fallback-style=none

echo "formatting rtos folder..."
find core/rtos -name "*.h" -o -name "*.c" | xargs clang-format -i -style=file -fallback-style=none

CHANGES=`git diff`

if ! test -z "$CHANGES"; then
    echo "  Pipeline will fail if you push unformatted code."
    echo "  You can bypass this check locally with:"
    echo ""
    echo "  git commit --no-verify"
    echo ""
	exit 1
else
    echo "  Code formatted correctly."
    echo ""
fi

exit 0