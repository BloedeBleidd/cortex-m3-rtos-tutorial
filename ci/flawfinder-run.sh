#!/bin/bash

# Configuration begin ---------------------------

SCAN_PATH="core"
MIN_LEVEL=0
REPORT_DEST="flawfinder-report"

# Configuration end ---------------------------

COMMAND="flawfinder --minlevel=$MIN_LEVEL --context --immediate"
CHECK_COMMAND="$COMMAND $SCAN_PATH"
HTML_COMMAND="$COMMAND --html $SCAN_PATH"
CSV_COMMAND="$COMMAND --csv $SCAN_PATH"

echo ""
echo "Executing FlawFinder"
echo "$CHECK_COMMAND"
CHECK=$( eval "$CHECK_COMMAND" )
echo "$CHECK"
mkdir -p $REPORT_DEST
echo ""
echo "Generating HTML output"
echo "$HTML_COMMAND"
HTML=$( eval "$HTML_COMMAND > $REPORT_DEST/$REPORT_DEST.html" )
echo "HTML report generated"
echo ""
echo "Generating CSV output"
echo "$CSV_COMMAND"
HTML=$( eval "$CSV_COMMAND > $REPORT_DEST/$REPORT_DEST.csv" )
echo "CSV report generated"

if [[ $CHECK == *"No hits found."* ]]; then
    echo ""
    echo "The code is fine :)"
    exit 0
fi

exit 1