#!/bin/bash

echo "formatting code folder..."
find core/code -name *.h -o -name *.c -o -name *.cpp -o -name *.hpp | xargs clang-format -i -style=file -fallback-style=none

echo "formatting driver folder..."
find core/driver -name *.h -o -name *.c -o -name *.cpp -o -name *.hpp | xargs clang-format -i -style=file -fallback-style=none

echo "formatting rtos folder..."
find core/rtos -name *.h -o -name *.c -o -name *.cpp -o -name *.hpp | xargs clang-format -i -style=file -fallback-style=none

CHANGES=`git diff`

if ! test -z "$CHANGES"; then
	echo ""
	echo "changes proceed during clang-format process:"
	echo "$CHANGES"
	exit 1
fi

exit 0